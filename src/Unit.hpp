#ifndef MCS_UNIT_HPP
#define MCS_UNIT_HPP

#include <memory>

namespace mcs {
class Unit {
public:
  static std::shared_ptr<Unit> instance;

  Unit() = default;

  static auto get() -> Unit *;
  auto operator==(const Unit &rhs) -> bool;
  auto operator!=(const Unit &rhs) -> bool;
};
} // namespace mcs

#endif // MCS_UNIT_HPP
