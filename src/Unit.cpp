#include "Unit.hpp"

std::shared_ptr<mcs::Unit> mcs::Unit::instance = std::make_shared<mcs::Unit>();

auto mcs::Unit::operator==(const mcs::Unit &rhs) -> bool { return true; }

auto mcs::Unit::operator!=(const mcs::Unit &rhs) -> bool { return false; }

auto mcs::Unit::get() -> mcs::Unit * {
  return instance.get();
}
