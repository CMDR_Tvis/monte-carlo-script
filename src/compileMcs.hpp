#ifndef MCS_COMPILEMCS_HPP
#define MCS_COMPILEMCS_HPP

#include "BytecodeGenerator.hpp"
#include "Result.hpp"
#include "RecordingErrorListener.hpp"
#include <vector>
#include <McsLexer.h>

namespace mcs {
auto compileMcs(const std::string &expression,
                BytecodeGenerator *generator = new BytecodeGenerator())
    -> Result<std::vector<byte>>;
} // namespace mcs

#endif // MCS_COMPILEMCS_HPP
