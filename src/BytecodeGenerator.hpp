#ifndef MCS_MCSEVALUATOR_HPP
#define MCS_MCSEVALUATOR_HPP

#include "BuiltinFunctions.hpp"
#include "Opcodes.hpp"
#include "Result.hpp"
#include <McsParser.h>
#include <ParserRuleContext.h>
#include <cstring>

namespace mcs {
class BytecodeGenerator final {
  BuiltinFunctions *_functions = new BuiltinFunctions();
  std::vector<byte> *_tail = new std::vector<byte>();
  byte _quantityOfDeclarations = 0_byte;

  std::map<long double, byte> *_declarations =
      new std::map<long double, byte>();

  std::mutex _mutex = std::mutex();

public:
  ~BytecodeGenerator();

  BytecodeGenerator();

private:
  static auto evaluateSign(McsParser::SignContext &ctx) -> short int;

  auto appendNumberRef(long double number);

  auto appendArgument(McsParser::ArgumentContext &ctx) -> void;

  auto appendFunctionRef(std::string &name, size_t arity);

  auto appendNumber(McsParser::NumberContext &ctx) -> Result<>;

  auto appendPi(McsParser::PiContext &ctx) -> void;

  auto appendE(McsParser::EContext &ctx) -> void;

  auto appendExpression(McsParser::ExpressionContext &ctx) -> Result<>;

  auto appendExponentiation(McsParser::ExponentiationContext &ctx) -> Result<>;

  auto appendFunctionCall(McsParser::FunctionCallContext &ctx) -> Result<>;

  auto appendMultiplication(McsParser::MultiplicationContext &ctx) -> Result<>;

  auto appendDivision(McsParser::DivisionContext &ctx) -> Result<>;

  auto appendSubtraction(McsParser::SubtractionContext &ctx) -> Result<>;

  auto appendAddition(McsParser::AdditionContext &ctx) -> Result<>;

  auto appendSubexpression(McsParser::SubexpressionContext &ctx) -> Result<>;

  auto appendDeclarations() -> void;

public:
  auto generateRoot(McsParser::ProgramContext &ctx)
      -> Result<std::vector<byte>>;
};
} // namespace mcs

#endif // MCS_MCSEVALUATOR_HPP
