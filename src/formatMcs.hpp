#ifndef MCS_INTERPRETMCS_HPP
#define MCS_INTERPRETMCS_HPP

#include "BuiltinFunctions.hpp"
#include "Opcodes.hpp"
#include <stack>
#include <ostream>
#include <vector>

namespace mcs {

enum FormatAction {
  COLLECT_NUMBER_REF,
  COLLECT_FUNCTION_REF,
  COLLECT_DECLARATIONS_SIZE,
  COLLECT_DECLARATIONS,
  COLLECT_OPCODE,
};

class FormatState {
public:
  FormatAction action;
  size_t bytesTillEnd = static_cast<size_t>(1ul);

  explicit FormatState(const FormatAction &action);
  FormatState(const FormatAction &action, const size_t &bytesTillEnd);
};

auto printOpcode(const byte &i,
                 std::ostream &out) -> FormatState;

auto formatMcs(std::vector<byte> &bytes, std::ostream &out,
               BuiltinFunctions *functions = new BuiltinFunctions())
    -> void;
} // namespace mcs
#endif // MCS_INTERPRETEMCS_HPP
