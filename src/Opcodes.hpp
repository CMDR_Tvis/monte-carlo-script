#ifndef MCS_OPCODES_HPP
#define MCS_OPCODES_HPP
#include <cstring>
#include <string>
#include <vector>

namespace mcs {
typedef uint_fast8_t byte;

constexpr byte operator"" _byte(unsigned long long ll) {
    return static_cast<byte>(ll);
}

enum Opcodes {
  DECLARATIONS = 0_byte,
  DOUBLE = 1_byte,
  X = 2_byte,
  ADD = 3_byte,
  SUB = 4_byte,
  EXP = 5_byte,
  DIV = 6_byte,
  MULT = 7_byte,
  INVOKE = 8_byte,
};
} // namespace mcs
#endif // MCS_OPCODES_HPP
