#include "compileMcs.hpp"

auto mcs::compileMcs(const std::string &expression,
                     BytecodeGenerator *generator)
    -> Result<std::vector<byte>> {
  const auto inputStream =
      std::make_shared<antlr4::ANTLRInputStream>(expression);

  const auto lexer = std::make_shared<McsLexer>(inputStream.get());
  lexer->removeErrorListeners();
  const auto recorder = std::make_shared<RecordingErrorListener>();
  lexer->addErrorListener(recorder.get());

  const auto tokenStream =
      std::make_shared<antlr4::CommonTokenStream>(lexer.get());

  const auto parser = std::make_shared<McsParser>(tokenStream.get());
  parser->removeErrorListeners();
  parser->addErrorListener(recorder.get());
  const auto program = parser->program();
  return generator->generateRoot(*program);
}
