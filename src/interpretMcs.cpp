#include "interpretMcs.hpp"

template <typename T> auto mcs::getAndPop(std::stack<T> *stack) -> T {
  auto r = stack->top();
  stack->pop();
  return r;
}

inline auto mcs::collectOpcode(const long double &x, const byte &i,
                        std::stack<long double> *memstack) -> State {
  long double p1, p2;

  switch (i) {
  case Opcodes::DECLARATIONS:
    return State(COLLECT_DECLARATIONS_SIZE);
  case Opcodes::DOUBLE:
    return State(COLLECT_NUMBER_REF);
  case Opcodes::X:
    memstack->push(x);
    break;
  case Opcodes::ADD:
    p1 = getAndPop(memstack);
    p2 = getAndPop(memstack);
    memstack->push(p1 + p2);
    break;
  case Opcodes::SUB:
    p1 = getAndPop(memstack);
    p2 = getAndPop(memstack);
    memstack->push(p2 - p1);
    break;
  case Opcodes::EXP:
    p1 = getAndPop(memstack);
    p2 = getAndPop(memstack);
    memstack->push(std::pow(p2, p1));
    break;
  case Opcodes::DIV:
    p1 = getAndPop(memstack);
    p2 = getAndPop(memstack);
    memstack->push(p2 / p1);
    break;
  case Opcodes::MULT:
    p1 = getAndPop(memstack);
    p2 = getAndPop(memstack);
    memstack->push(p1 * p2);
    break;
  case Opcodes::INVOKE:
    return State(COLLECT_FUNCTION_REF);

  default:
    break;
  }

  return State(COLLECT_OPCODE);
}

auto mcs::interpretMcs(const long double &x, std::vector<byte> &bytes,
                       std::vector<long double> *declarations,
                       BuiltinFunctions *functions) -> long double {
  auto memoryStack = new std::stack<long double>();
  auto collection = std::vector<byte>();
  byte functionArity;
  auto args = std::vector<long double>();
  auto state = State(COLLECT_OPCODE);
  long double d;
  byte quantityOfDeclarations;
  std::size_t n;

  for (auto iByte : bytes) {
    switch (state.action) {
    case SKIP:
      state.bytesTillEnd--;

      if (state.bytesTillEnd == static_cast<size_t>(0ul))
        state = State(COLLECT_OPCODE);

      break;
    case COLLECT_NUMBER_REF:
      memoryStack->push(declarations->at((size_t)iByte));
      state = State(COLLECT_OPCODE);
      break;
    case COLLECT_FUNCTION_REF:
      functionArity = functions->arityOf(iByte);
      args.reserve(functionArity);

      for (auto i = 0_byte; i < functionArity; i++)
        args.push_back(getAndPop(memoryStack));

      memoryStack->push(functions->invoke(iByte, args));
      args.clear();
      state = State(COLLECT_OPCODE);
      break;
    case COLLECT_DECLARATIONS_SIZE:
      quantityOfDeclarations = iByte;
      n = sizeof(long double) * quantityOfDeclarations;
      collection.reserve(n);

      if (!declarations->empty()) {
        state = {SKIP, n};
        break;
      }

      state = {COLLECT_DECLARATIONS, n};
      break;
    case COLLECT_DECLARATIONS:
      collection.push_back(iByte);
      state.bytesTillEnd--;

      if (state.bytesTillEnd == 0) {
        declarations->reserve(quantityOfDeclarations);

        for (auto i = static_cast<size_t>(0ul); i < n;
             i += sizeof(long double)) {
          byte result[sizeof(long double)];

          std::copy(collection.begin() + i,
                    collection.begin() + i + sizeof(long double), result);

          memcpy(&d, result, sizeof(d));
          declarations->push_back(d);
        }

        collection.clear();
        state = State(COLLECT_OPCODE);
      }

      break;

    case COLLECT_OPCODE:
      state = collectOpcode(x, iByte, memoryStack);
      break;
    }
  }

  auto top = memoryStack->top();
  delete memoryStack;
  return top;
}

mcs::State::State(const mcs::Action &action, const size_t &bytesTillEnd) {
  this->action = action;
  this->bytesTillEnd = bytesTillEnd;
}

mcs::State::State(const mcs::Action &action) { this->action = action; }
