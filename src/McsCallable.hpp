#ifndef MCS_MCSCALLABLE_HPP
#define MCS_MCSCALLABLE_HPP

#include "Opcodes.hpp"
#include <string>
#include <vector>

namespace mcs {
class McsCallable {
public:
  virtual ~McsCallable() = default;
  virtual auto call(std::vector<long double> &args) -> long double = 0;
  virtual auto arity() -> byte = 0;
  virtual auto name() -> std::string = 0;
};
} // namespace mcs
#endif // MCS_MCSCALLABLE_HPP
