#ifndef MCS_RECORDINGERRORLISTENER_HPP
#define MCS_RECORDINGERRORLISTENER_HPP
#include <BaseErrorListener.h>
#include <Recognizer.h>
#include <Token.h>

namespace mcs {
class RecordingErrorListener final : public antlr4::BaseErrorListener {
public:
  std::vector<std::string> *tail = new std::vector<std::string>();

  ~RecordingErrorListener() override;

  auto syntaxError(antlr4::Recognizer *recognizer,
                   antlr4::Token *offendingSymbol, size_t line,
                   size_t charPositionInLine, const std::string &msg,
                   std::exception_ptr e) -> void override;
};
} // namespace mcs
#endif // MCS_RECORDINGERRORLISTENER_HPP
