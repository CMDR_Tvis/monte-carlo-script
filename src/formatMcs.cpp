#include "formatMcs.hpp"

inline auto mcs::printOpcode(const byte &i, std::ostream &out) -> FormatState {
  switch (i) {
  case Opcodes::DECLARATIONS:
    out << "DECLARATIONS" << std::endl;
    return FormatState(COLLECT_DECLARATIONS_SIZE);
  case Opcodes::DOUBLE:
    out << "DOUBLE ";
    return FormatState(COLLECT_NUMBER_REF);
  case Opcodes::X:
    out << "X" << std::endl;
    break;
  case Opcodes::ADD:
    out << "ADD" << std::endl;
    break;
  case Opcodes::SUB:
    out << "SUB" << std::endl;
    break;
  case Opcodes::EXP:
    out << "EXP" << std::endl;
    break;
  case Opcodes::DIV:
    out << "DIV" << std::endl;
    break;
  case Opcodes::MULT:
    out << "MULT" << std::endl;
    break;
  case Opcodes::INVOKE:
    out << "INVOKE ";
    return FormatState(COLLECT_FUNCTION_REF);

  default:
    break;
  }

  return FormatState(COLLECT_OPCODE);
}

auto mcs::formatMcs(std::vector<byte> &bytes, std::ostream &out,
                    BuiltinFunctions *functions) -> void {
  auto declarations = std::vector<long double>();
  auto collection = new std::vector<byte>();
  auto state = FormatState(COLLECT_OPCODE);
  long double d;
  byte quantityOfDeclarations;
  std::size_t n;

  for (auto iByte : bytes) {
    switch (state.action) {

    case COLLECT_NUMBER_REF:
      out << declarations.at(iByte) << std::endl;
      state = FormatState(COLLECT_OPCODE);
      break;
    case COLLECT_FUNCTION_REF:
      out << functions->nameOf(iByte) << ' ' << (int)functions->arityOf(iByte)
          << std::endl;

      state = FormatState(COLLECT_OPCODE);
      break;
    case COLLECT_DECLARATIONS_SIZE:
      quantityOfDeclarations = iByte;
      n = sizeof(long double) * quantityOfDeclarations;
      collection->reserve(n);
      state = {COLLECT_DECLARATIONS, n};
      break;
    case COLLECT_DECLARATIONS:
      collection->push_back(iByte);
      state.bytesTillEnd--;

      if (state.bytesTillEnd == static_cast<size_t>(0ul)) {
        declarations.reserve(quantityOfDeclarations);

        for (auto i = static_cast<size_t>(0ul); i < n;
             i += sizeof(long double)) {
          byte result[sizeof(long double)];

          std::copy(collection->begin() + i,
                    collection->begin() + i + sizeof(long double), result);

          memcpy(&d, result, sizeof(d));
          out << d << std::endl;
          declarations.push_back(d);
        }

        collection->clear();
        state = FormatState(COLLECT_OPCODE);
      }

      break;

    case COLLECT_OPCODE:
      state = printOpcode(iByte, out);
      break;
    }
  }

  delete collection;
}

mcs::FormatState::FormatState(const mcs::FormatAction &action,
                              const size_t &bytesTillEnd) {
  this->action = action;
  this->bytesTillEnd = bytesTillEnd;
}

mcs::FormatState::FormatState(const mcs::FormatAction &action) {
  this->action = action;
}
