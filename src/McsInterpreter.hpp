#ifndef MCS_MCSINTERPRETER_HPP
#define MCS_MCSINTERPRETER_HPP
#include "BuiltinFunctions.hpp"
#include "compileMcs.hpp"
#include "interpretMcs.hpp"
#include "Result.hpp"
#include <mutex>
#include <thread>
#include <map>

namespace mcs {
class McsInterpreter final {
  std::mutex _mutexCollect = std::mutex();
  std::mutex _mutexHistogram = std::mutex();

  std::unordered_map<unsigned long long int, unsigned long long int> *_hits =
      new std::unordered_map<unsigned long long int, unsigned long long int>();

  std::vector<std::thread *> *_workers = nullptr;
  const unsigned short int _parallelism;
  BuiltinFunctions *_functions = new BuiltinFunctions();

  auto
  collect(std::unordered_map<unsigned long long, unsigned long long> &bucket);

  static auto random(long double min, long double max);

public:
  [[nodiscard]] static auto plot(const std::string &expression,
                                 long double fromInclusive,
                                 long double toExclusive, std::size_t nPoints)
      -> Result<std::map<long double, long double>>;

  auto histogram(const std::string &expression, unsigned long long int nValues,
                 long double xMin, long double xMax, long double histogramMin,
                 long double histogramMax, std::size_t nBins)
      -> Result<std::vector<long double>>;

  static auto evaluate(const std::string &expression, long double x)
      -> Result<const long double>;

  explicit McsInterpreter(unsigned short int parallelism);

  ~McsInterpreter();
};
} // namespace mcs

#endif // MCS_MCSINTERPRETER_HPP
