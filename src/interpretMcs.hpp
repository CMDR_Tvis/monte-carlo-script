#ifndef MCS_INTERPRETMCS_HPP
#define MCS_INTERPRETMCS_HPP

#include "BuiltinFunctions.hpp"
#include "Opcodes.hpp"
#include <stack>
#include <vector>

namespace mcs {

enum Action {
  COLLECT_NUMBER_REF,
  COLLECT_FUNCTION_REF,
  COLLECT_DECLARATIONS_SIZE,
  COLLECT_DECLARATIONS,
  COLLECT_OPCODE,
  SKIP
};

class State {
public:
  Action action;
  size_t bytesTillEnd = 1;

  explicit State(const Action &action);
  State(const Action &action, const size_t &bytesTillEnd);
};

template <typename T> auto getAndPop(std::stack<T> *stack) -> T;

auto collectOpcode(const long double &x, const byte &i,
                   std::stack<long double> *memstack) -> State;

auto interpretMcs(
    const long double &x, std::vector<byte> &bytes,
    std::vector<long double> *declarations = new std::vector<long double>(),
    BuiltinFunctions *functions = new BuiltinFunctions()) -> long double;
} // namespace mcs
#endif // MCS_INTERPRETEMCS_HPP
