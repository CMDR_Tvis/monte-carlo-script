#ifndef MCS_RESULT_HPP
#define MCS_RESULT_HPP
#include "Unit.hpp"
#include <functional>
#include <string>
#include <vector>

namespace mcs {
template <typename T = mcs::Unit> class Result {
  std::shared_ptr<std::vector<std::string>> errors = nullptr;
  std::shared_ptr<T> value = nullptr;

  explicit Result(std::vector<std::string> *errors) {
    this->errors = std::shared_ptr<std::vector<std::string>>(errors);
  };

  explicit Result(T *value) { this->value = std::shared_ptr<T>(value); };

public:
  ~Result() = default;

  [[nodiscard]] auto hasErrors() const -> bool { return errors != nullptr; }

  [[nodiscard]] auto hasValue() const -> bool { return value != nullptr; }

  [[nodiscard]] auto errorsOrNull() const -> std::vector<std::string> * {
    return errors.get();
  }

  [[nodiscard]] auto valueOrNull() const -> T * { return value.get(); }

  auto ifHasErrors(
      std::function<void(const std::vector<std::string> *)> &action) const
      -> void {
    if (errors)
      action(errors.get());
  }

  auto ifHasValue(const std::function<void(const T *)>) const -> void {
    if (value)
      action(value.get());
  }

  static auto ofErrors(std::vector<std::string> *errors) -> Result<T> {
    return Result<T>(errors);
  }

  static auto ofErrors(const std::string &error) -> Result<T> {
    return Result<T>(new std::vector<std::string>{error});
  }

  static auto ofValue(T *value) -> Result<T> { return Result<T>(value); }
};
template <> class Result<mcs::Unit> {
  std::shared_ptr<std::vector<std::string>> errors = nullptr;
  std::shared_ptr<mcs::Unit> value = nullptr;

  explicit Result(std::vector<std::string> *errors) {
    this->errors = std::shared_ptr<std::vector<std::string>>(errors);
  };

  explicit Result(mcs::Unit * = nullptr) { this->value = mcs::Unit::instance; };

public:
  [[nodiscard]] auto hasErrors() const -> bool { return errors != nullptr; }

  [[nodiscard]] auto hasValue() const -> bool { return value != nullptr; }

  [[nodiscard]] auto errorsOrNull() const -> std::vector<std::string> * {
    return errors.get();
  }

  [[nodiscard]] auto valueOrNull() const -> Unit * { return value.get(); }

  static auto ofErrors(std::vector<std::string> *errors) -> Result<Unit> {
    return Result<Unit>(errors);
  }

  static auto ofErrors(const std::string &error) -> Result<Unit> {
    return Result<Unit>(new std::vector<std::string>{error});
  }

  static auto ofValue(Unit * = nullptr) -> Result<Unit> {
    return Result<Unit>();
  }
};
} // namespace mcs
#endif // MCS_RESULT_HPP
