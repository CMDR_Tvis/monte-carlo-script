#ifndef MCS_BUILTINFUNCTIONS_HPP
#define MCS_BUILTINFUNCTIONS_HPP

#include "McsFunction.hpp"
#include <cmath>
#include <functional>
#include <memory>
#include <string>
#include <unordered_map>
#include <vector>

namespace mcs {

class BuiltinFunctions final {
  const std::vector<McsCallable *> *_functions = new std::vector<McsCallable *>{
      new McsFunction<>(
          "abs",
          [](std::array<long double, 1> args) { return std::abs(args[0]); }),

      new McsFunction<>(
          "acos",
          [](std::array<long double, 1> args) { return std::acos(args[0]); }),

      new McsFunction<>("acot",
                        [](std::array<long double, 1> args) {
                          return std::atan(1.0l / args[0]);
                        }),

      new McsFunction<>("asec",
                        [](std::array<long double, 1> args) {
                          return std::acos(1.0l / args[0]);
                        }),

      new McsFunction<>(
          "asin",
          [](std::array<long double, 1> args) { return std::asin(args[0]); }),

      new McsFunction<>(
          "atan",
          [](std::array<long double, 1> args) { return std::atan(args[0]); }),

      new McsFunction<>(
          "cbrt",
          [](std::array<long double, 1> args) { return std::cbrt(args[0]); }),

      new McsFunction<>(
          "cos",
          [](std::array<long double, 1> args) { return std::cos(args[0]); }),

      new McsFunction<>("cot",
                        [](std::array<long double, 1> args) {
                          const auto x = args[0];
                          return std::cos(x) / std::sin(x);
                        }),

      new McsFunction<>("exp",
                        [](std::array<long double, 1> args) {
                          return std::exp(args[0]);
                        }),

      new McsFunction<>("h",
                        [](std::array<long double, 1> args) {
                          if (args[0] >= 0.0l)
                            return 1.0l;

                          return 0.0l;
                        }),

      new McsFunction<2>("log",
                         [](std::array<long double, 2> args) {
                           return std::log(args[1]) / std::log(args[0]);
                         }),

      new McsFunction<>(
          "log10",
          [](std::array<long double, 1> args) { return std::log10(args[0]); }),

      new McsFunction<>(
          "log2",
          [](std::array<long double, 1> args) { return std::log2(args[0]); }),

      new McsFunction<>(
          "ln",
          [](std::array<long double, 1> args) { return std::log(args[0]); }),

      new McsFunction<2>("root",
                         [](std::array<long double, 2> args) {
                           return std::pow(args[1], 1.0l / args[0]);
                         }),

      new McsFunction<>("sec",
                        [](std::array<long double, 1> args) {
                          return 1.0l / std::cos(args[0]);
                        }),

      new McsFunction<>(
          "sin",
          [](std::array<long double, 1> args) { return std::sin(args[0]); }),

      new McsFunction<>(
          "sqrt",
          [](std::array<long double, 1> args) { return std::sqrt(args[0]); }),

      new McsFunction<>(
          "tan",
          [](std::array<long double, 1> args) { return std::tan(args[0]); }),
  };

public:
  BuiltinFunctions() = default;
  ~BuiltinFunctions() { delete _functions; };

  auto invoke(const byte &index, std::vector<long double> &args)
      -> long double {
    return _functions->at(index)->call(args);
  }

  auto indexOf(const std::string &function, const std::size_t &arity) -> byte {
    auto idx = 0_byte;

    for (auto it = _functions->begin(); it != _functions->end(); ++it, ++idx) {
      auto f = *it;

      if (f->name() == function && f->arity() == arity)
        break;
    }

    return idx;
  }

  auto arityOf(const byte &index) -> byte {
    return _functions->at(index)->arity();
  }

  auto nameOf(const byte &index) -> std::string {
    return _functions->at(index)->name();
  }

  auto exists(const std::string &function, const std::size_t &arity) {
    return std::find_if(_functions->begin(), _functions->end(),
                        [function, &arity](McsCallable *it) {
                          return it->name() == function && it->arity() == arity;
                        }) != _functions->end();
  }
};
} // namespace mcs

#endif // MCS_BUILTINFUNCTIONS_HPP
