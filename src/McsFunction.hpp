#ifndef MCS_MCSFUNCTION_HPP
#define MCS_MCSFUNCTION_HPP

#include "McsCallable.hpp"
#include <cstdlib>
#include <functional>
#include <string>
#include <utility>

namespace mcs {
template <byte Arity = 1_byte> class McsFunction final : public McsCallable {
  std::string _name;
  std::function<long double(std::array<long double, Arity>)> _implementation;

public:
  McsFunction(std::string name,
              const std::function<long double(std::array<long double, Arity>)>
                  &implementation)
      : _name(std::move(name)), _implementation(implementation) {}

  auto call(std::vector<long double> &args) -> long double override {
    auto arr = std::array<long double, Arity>();

    std::copy_n(std::make_move_iterator(args.begin()), Arity, arr.begin());
    return _implementation(arr);
  }

  auto arity() -> byte override { return Arity; }
  auto name() -> std::string override { return _name; }
};
} // namespace mcs

#endif // MCS_MCSFUNCTION_HPP
