#include "McsInterpreter.hpp"

mcs::McsInterpreter::McsInterpreter(const unsigned short int parallelism)
    : _parallelism(parallelism) {}

mcs::McsInterpreter::~McsInterpreter() {
  delete _hits;
  delete _workers;
  delete _functions;
}

auto mcs::McsInterpreter::collect(
    std::unordered_map<unsigned long long int, unsigned long long int>
        &bucket) {
  _mutexCollect.lock();

  std::for_each(bucket.begin(), bucket.end(),
                [this](std::pair<unsigned long long int, long double> p) {
                  if (_hits->count(p.first) > 0)
                    _hits->insert_or_assign(p.first,
                                            p.second + _hits->at(p.first));
                  else
                    _hits->insert_or_assign(p.first, p.second);
                });

  _mutexCollect.unlock();
}

auto mcs::McsInterpreter::plot(const std::string &expression,
                               const long double fromInclusive,
                               const long double toExclusive,
                               const std::size_t nPoints)
    -> Result<std::map<long double, long double>> {
  if (toExclusive < fromInclusive)
    return Result<std::map<long double, long double>>::ofErrors(
        "toExclusive cannot be less than fromInclusive");

  const auto bytes = compileMcs(expression);

  if (bytes.hasErrors())
    return Result<std::map<long double, long double>>::ofErrors(
        bytes.errorsOrNull());

  const auto declarations = std::make_shared<std::vector<long double>>();
  const auto plot = new std::map<long double, long double>{};

  const auto increment =
      (toExclusive - fromInclusive) / static_cast<long double>(nPoints);

  auto _functions = std::make_shared<BuiltinFunctions>();

  for (auto i = 0ull; i < nPoints; i++) {
    auto x = fromInclusive + i * increment;

    auto y = interpretMcs(x, *bytes.valueOrNull(), declarations.get(),
                          _functions.get());

    plot->insert_or_assign(x, y);
  }

  return Result<std::map<long double, long double>>::ofValue(plot);
}

auto mcs::McsInterpreter::random(const long double min, const long double max) {
  static std::array<long long int, 2> u = {1, 0};

  static const std::array<long long int, 2> m = {45887173, 11368};

  static const std::array<double, 2> x = {std::pow(2.0, -40.0),
                                          std::pow(2.0, -14.0)};

  long long int c1;
  long long int c0;
  long long int n;
  c0 = m[0] * u[0];
  c1 = m[0] * u[1] + m[1] * u[0];
  u[0] = c0 - ((c0 >> 26) << 26);
  n = c1 + (c0 >> 26);
  u[1] = n - ((n >> 14) << 14);
  return min + (u[0] * x[0] + u[1] * x[1]) * (max - min);
}

auto mcs::McsInterpreter::histogram(const std::string &expression,
                                    const unsigned long long int nValues,
                                    long double xMin, long double xMax,
                                    const long double histogramMin,
                                    const long double histogramMax,
                                    const std::size_t nBins)
    -> Result<std::vector<long double>> {
  _mutexHistogram.lock();
  _workers = new std::vector<std::thread *>();
  _hits->clear();
  const auto bytes = compileMcs(expression);

  if (bytes.hasErrors())
    return mcs::Result<std::vector<long double>>::ofErrors(
        bytes.errorsOrNull());

  const auto portionSize = nValues / _parallelism;
  const auto fixingPortionSize = portionSize + nValues % _parallelism;
  auto portion = fixingPortionSize;
  auto binBoundaries = std::vector<long double>{histogramMin};

  const auto intervalIncrement =
      (histogramMax - histogramMin) / static_cast<long double>(nBins);

  for (auto i = 0ull; i < nBins; i++)
    binBoundaries.push_back(binBoundaries.back() + intervalIncrement);

  for (auto i = 0u; i < _parallelism; i++) {
    _workers->push_back(
        new std::thread([bytes, &portion, this, &xMin, binBoundaries, &xMax]() {
          const auto bucket = new std::unordered_map<unsigned long long int,
                                                     unsigned long long int>();

          auto declarations = std::make_shared<std::vector<long double>>();

          for (auto j = 0ull; j < portion; j++) {
            const auto value =
                interpretMcs(random(xMin, xMax), *bytes.valueOrNull(),
                             declarations.get(), _functions);

            auto idx = static_cast<std::size_t>(0ul);

            for (auto it = binBoundaries.begin(); it != binBoundaries.end();
                 ++it, ++idx) {
              if (value > *it && value < *(it + 1)) {
                if (bucket->count(idx) > 0)
                  bucket->insert_or_assign(idx, bucket->at(idx) + 1ull);
                else
                  bucket->insert_or_assign(idx, 1ull);

                break;
              }
            }
          }

          collect(*bucket);
          delete bucket;
        }));

    portion = portionSize;
  }

  std::for_each(_workers->begin(), _workers->end(),
                [](std::thread *t) { t->join(); });

  auto sum = 0ull;

  std::for_each(
      _hits->begin(), _hits->end(),
      [&sum](
          std::pair<unsigned long long int, unsigned long long int> p) mutable {
        sum = sum + p.second;
      });

  auto bins = std::vector<long double>();
  bins.reserve(nBins);

  for (auto i = 0ull; i < nBins; i++) {
    if (_hits->count(i) == 0)
      bins.push_back(0);
    else
      bins.push_back(static_cast<long double>(_hits->at(i)) / sum);
  }

  auto binsPtr = new std::vector(bins);

  std::for_each(_workers->begin(), _workers->end(),
                [](std::thread *t) { delete t; });

  delete _workers;
  _workers = nullptr;
  _mutexHistogram.unlock();
  return Result<std::vector<long double>>::ofValue(binsPtr);
}

auto mcs::McsInterpreter::evaluate(const std::string &expression,
                                   const long double x)
    -> Result<const long double> {

  const auto bytes = compileMcs(expression);

  if (bytes.hasErrors())
    return Result<const long double>::ofErrors(bytes.errorsOrNull());

  return Result<const long double>::ofValue(
      new long double(interpretMcs(x, *bytes.valueOrNull())));
}
