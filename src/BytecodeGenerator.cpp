#include "BytecodeGenerator.hpp"

#ifndef M_PIl
#define M_PIl 3.141592653589793238462643383279502884L
#endif
#ifndef M_El
#define M_El 2.718281828459045235360287471352662498L
#endif

mcs::BytecodeGenerator::BytecodeGenerator() = default;

mcs::BytecodeGenerator::~BytecodeGenerator() { delete _functions; }

auto mcs::BytecodeGenerator::appendNumberRef(const long double number) {
  if (_declarations->count(number) == 0) {
    _declarations->insert_or_assign(number, _quantityOfDeclarations);
    _tail->push_back(_quantityOfDeclarations);
    _quantityOfDeclarations++;
    return;
  }

  _tail->push_back(_declarations->at(number));
}

auto mcs::BytecodeGenerator::appendArgument(McsParser::ArgumentContext &ctx)
    -> void {
  auto sign = evaluateSign(*ctx.sign());

  if (sign == -1) {
    _tail->push_back(Opcodes::DOUBLE);
    appendNumberRef(sign);
  }

  _tail->push_back(Opcodes::X);

  if (sign == -1)
    _tail->push_back(Opcodes::MULT);
}

auto mcs::BytecodeGenerator::appendFunctionRef(std::string &name,
                                               size_t arity) {
  _tail->push_back((byte)_functions->indexOf(name, arity));
}

auto mcs::BytecodeGenerator::evaluateSign(McsParser::SignContext &ctx)
    -> short int {
  auto sign = ctx.getText();

  if (std::count(sign.begin(), sign.end(), '-') % 2 == 0)
    return 1;

  return -1;
}

auto mcs::BytecodeGenerator::appendNumber(McsParser::NumberContext &ctx)
    -> Result<> {
  auto sign = evaluateSign(*ctx.sign());

  if (sign == -1) {
    _tail->push_back(Opcodes::DOUBLE);
    appendNumberRef(sign);
  }

  _tail->push_back(Opcodes::DOUBLE);

  auto text = ctx.DOUBLE()->getText();

  try {
    appendNumberRef(std::stold(text));
  } catch (...) {
    return Result<>::ofErrors(
        new std::vector<std::string>{"Cannot parse " + text + " as a number."});
  }

  if (sign == -1)
    _tail->push_back(Opcodes::MULT);

  return Result<>::ofValue();
}

auto mcs::BytecodeGenerator::appendPi(McsParser::PiContext &ctx) -> void {
  const auto sign = evaluateSign(*ctx.sign());

  if (sign == -1) {
    _tail->push_back(Opcodes::DOUBLE);
    appendNumberRef(sign);
  }

  _tail->push_back(Opcodes::DOUBLE);
  appendNumberRef(M_PIl);

  if (sign == -1)
    _tail->push_back(Opcodes::MULT);
}

auto mcs::BytecodeGenerator::appendE(McsParser::EContext &ctx) -> void {
  const auto sign = evaluateSign(*ctx.sign());

  if (sign == -1) {
    _tail->push_back(Opcodes::DOUBLE);
    appendNumberRef(sign);
  }

  _tail->push_back(Opcodes::DOUBLE);
  appendNumberRef(M_El);

  if (sign == -1)
    _tail->push_back(Opcodes::MULT);
}

auto mcs::BytecodeGenerator::appendExpression(McsParser::ExpressionContext &ctx)
    -> Result<> {
  if (const auto pow = dynamic_cast<McsParser::ExponentiationContext *>(&ctx))
    return appendExponentiation(*pow);

  if (const auto exponentiation =
          dynamic_cast<McsParser::MultiplicationContext *>(&ctx))
    return appendMultiplication(*exponentiation);

  if (const auto division = dynamic_cast<McsParser::DivisionContext *>(&ctx))
    return appendDivision(*division);

  if (const auto addition = dynamic_cast<McsParser::AdditionContext *>(&ctx))
    return appendAddition(*addition);

  if (const auto subtraction =
          dynamic_cast<McsParser::SubtractionContext *>(&ctx))
    return appendSubtraction(*subtraction);

  if (const auto number = dynamic_cast<McsParser::NumberContext *>(&ctx))
    return appendNumber(*number);

  if (const auto e = dynamic_cast<McsParser::EContext *>(&ctx)) {
    appendE(*e);
    return Result<>::ofValue();
  }

  if (const auto pi = dynamic_cast<McsParser::PiContext *>(&ctx)) {
    appendPi(*pi);
    return Result<>::ofValue();
  }

  if (const auto argumentCtx =
          dynamic_cast<McsParser::ArgumentContext *>(&ctx)) {
    appendArgument(*argumentCtx);
    return Result<>::ofValue();
  }

  if (const auto subexpression =
          dynamic_cast<McsParser::SubexpressionContext *>(&ctx))
    return appendSubexpression(*subexpression);

  if (const auto functionCall =
          dynamic_cast<McsParser::FunctionCallContext *>(&ctx))
    return appendFunctionCall(*functionCall);

  return Result<>::ofErrors("Illegal expression context: " + ctx.toString());
}

auto mcs::BytecodeGenerator::appendExponentiation(
    McsParser::ExponentiationContext &ctx) -> Result<> {
  auto errors = std::make_shared<std::vector<std::string>>();
  auto r1 = appendExpression(*ctx.expression(0));

  if (r1.hasErrors())
    errors->insert(errors->end(), r1.errorsOrNull()->begin(),
                   r1.errorsOrNull()->end());

  auto r2 = appendExpression(*ctx.expression(1));

  if (r2.hasErrors())
    errors->insert(errors->end(), r2.errorsOrNull()->begin(),
                   r2.errorsOrNull()->end());

  if (!errors->empty())
    return Result<>::ofErrors(errors.get());

  _tail->push_back(Opcodes::EXP);
  return Result<>::ofValue();
}

auto mcs::BytecodeGenerator::appendFunctionCall(
    McsParser::FunctionCallContext &ctx) -> Result<> {
  auto function = ctx.IDENTIFIER()->getText();
  auto arity = 0u;
  auto errors = std::make_shared<std::vector<std::string>>();

  for (auto argumentCtx : ctx.expression()) {
    if (!argumentCtx)
      break;

    arity++;
    auto r = appendExpression(*argumentCtx);

    if (r.hasErrors())
      errors->insert(errors->end(), r.errorsOrNull()->begin(),
                     r.errorsOrNull()->end());
  }

  if (!_functions->exists(function, arity)) {
    errors->push_back("function " + function + " with " +
                      std::to_string(arity) + " arguments doesn't exist");

    return Result<>::ofErrors(errors.get());
  }

  _tail->push_back(Opcodes::INVOKE);
  appendFunctionRef(function, arity);
  return Result<>::ofValue();
}

auto mcs::BytecodeGenerator::appendMultiplication(
    McsParser::MultiplicationContext &ctx) -> Result<> {
  auto errors = std::make_shared<std::vector<std::string>>();
  auto r1 = appendExpression(*ctx.expression(0));

  if (r1.hasErrors())
    errors->insert(errors->end(), r1.errorsOrNull()->begin(),
                   r1.errorsOrNull()->end());

  auto r2 = appendExpression(*ctx.expression(1));

  if (r2.hasErrors())
    errors->insert(errors->end(), r2.errorsOrNull()->begin(),
                   r2.errorsOrNull()->end());

  if (!errors->empty())
    return Result<>::ofErrors(errors.get());

  _tail->push_back(Opcodes::MULT);
  return Result<>::ofValue();
}

auto mcs::BytecodeGenerator::appendDivision(McsParser::DivisionContext &ctx)
    -> Result<> {
  auto errors = new std::vector<std::string>();
  auto r1 = appendExpression(*ctx.expression(0));

  if (r1.hasErrors())
    errors->insert(errors->end(), r1.errorsOrNull()->begin(),
                   r1.errorsOrNull()->end());

  auto r2 = appendExpression(*ctx.expression(1));

  if (r2.hasErrors())
    errors->insert(errors->end(), r2.errorsOrNull()->begin(),
                   r2.errorsOrNull()->end());

  if (!errors->empty())
    return Result<>::ofErrors(errors);

  _tail->push_back(Opcodes::DIV);
  return Result<>::ofValue();
}

auto mcs::BytecodeGenerator::appendSubtraction(
    McsParser::SubtractionContext &ctx) -> Result<> {

  auto errors = new std::vector<std::string>();
  auto r1 = appendExpression(*ctx.expression(0));

  if (r1.hasErrors())
    errors->insert(errors->end(), r1.errorsOrNull()->begin(),
                   r1.errorsOrNull()->end());

  auto r2 = appendExpression(*ctx.expression(1));

  if (r2.hasErrors())
    errors->insert(errors->end(), r2.errorsOrNull()->begin(),
                   r2.errorsOrNull()->end());

  if (!errors->empty())
    return Result<>::ofErrors(errors);

  _tail->push_back(Opcodes::SUB);
  return Result<>::ofValue();
}

auto mcs::BytecodeGenerator::appendAddition(McsParser::AdditionContext &ctx)
    -> Result<> {
  auto errors = new std::vector<std::string>();
  auto r1 = appendExpression(*ctx.expression(0));

  if (r1.hasErrors())
    errors->insert(errors->end(), r1.errorsOrNull()->begin(),
                   r1.errorsOrNull()->end());

  auto r2 = appendExpression(*ctx.expression(1));

  if (r2.hasErrors())
    errors->insert(errors->end(), r2.errorsOrNull()->begin(),
                   r2.errorsOrNull()->end());

  if (!errors->empty())
    return Result<>::ofErrors(errors);

  _tail->push_back(Opcodes::ADD);
  return Result<>::ofValue();
}

auto mcs::BytecodeGenerator::appendSubexpression(
    McsParser::SubexpressionContext &ctx) -> Result<> {
  auto sign = evaluateSign(*ctx.sign());

  if (sign == -1) {
    _tail->push_back(Opcodes::DOUBLE);
    appendNumberRef(sign);
  }

  auto r = appendExpression(*ctx.expression());

  if (sign == -1)
    _tail->push_back(Opcodes::MULT);

  return r;
}

auto mcs::BytecodeGenerator::generateRoot(McsParser::ProgramContext &ctx)
    -> Result<std::vector<byte>> {
  _mutex.lock();

  if (appendExpression(*ctx.expression()).hasErrors())
    return Result<std::vector<byte>>::ofErrors(
        appendExpression(*ctx.expression()).errorsOrNull());

  appendDeclarations();
  const auto value = new std::vector<byte>(*_tail);
  _tail->clear();
  _declarations->clear();
  _quantityOfDeclarations = 0_byte;
  _mutex.unlock();
  return Result<std::vector<byte>>::ofValue(value);
}

auto mcs::BytecodeGenerator::appendDeclarations() -> void {
  if (_quantityOfDeclarations == 0_byte)
    return;

  std::vector<byte> appendix{Opcodes::DECLARATIONS, _quantityOfDeclarations};
  std::vector<std::pair<long double, byte>> declarationsVector{};
  declarationsVector.reserve(_quantityOfDeclarations);

  for (const std::pair<const long double, byte> &p : *_declarations)
    declarationsVector.emplace_back(p);

  std::sort(declarationsVector.begin(), declarationsVector.end(),
            [](const std::pair<long double, byte> &lhs,
               const std::pair<long double, byte> &rhs) -> bool {
              return lhs.second < rhs.second;
            });

  for (const std::pair<long double, byte> &p : declarationsVector) {
    const auto d = p.first;
    byte result[sizeof(long double)];
    memcpy(result, &d, sizeof(d));
    appendix.insert(appendix.end(), std::begin(result), std::end(result));
  }

  _tail->insert(_tail->begin(), appendix.begin(), appendix.end());
}
