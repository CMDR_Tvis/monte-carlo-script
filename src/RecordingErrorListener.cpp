#include "RecordingErrorListener.hpp"

auto mcs::RecordingErrorListener::syntaxError(
    antlr4::Recognizer *recognizer, antlr4::Token *offendingSymbol, size_t line,
    size_t charPositionInLine, const std::string &msg, std::exception_ptr e)
-> void {
  tail->push_back("line " + std::to_string(line) + ':' +
                  std::to_string(charPositionInLine) + ' ' + msg);
}

mcs::RecordingErrorListener::~RecordingErrorListener() {
  delete tail;
}
