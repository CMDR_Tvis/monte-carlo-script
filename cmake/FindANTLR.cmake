cmake_minimum_required(VERSION 3.15.3)
find_package(Java QUIET COMPONENTS Runtime)

if (NOT ANTLR_EXECUTABLE)
    find_program(ANTLR_EXECUTABLE
            NAMES antlr.jar antlr4.jar antlr-4.jar antlr-4.8-complete.jar)
endif ()

macro(ADD_ANTLR_TARGET Name InputFile OutputDir Package)
    set(ANTLR_${Name}_OUTPUT_DIR ${OutputDir})
    get_filename_component(ANTLR_INPUT ${InputFile} NAME_WE)

    execute_process(COMMAND
            ${Java_JAVA_EXECUTABLE}
            -jar ${ANTLR_EXECUTABLE}
            -o ${ANTLR_${Name}_OUTPUT_DIR}
            -long-messages
            -Dlanguage=Cpp
            -package ${Package}
            ${InputFile})
endmacro(ADD_ANTLR_TARGET)

include(FindPackageHandleStandardArgs)

find_package_handle_standard_args(
        ANTLR
        REQUIRED_VARS ANTLR_EXECUTABLE Java_JAVA_EXECUTABLE
        VERSION_VAR ANTLR_VERSION)
