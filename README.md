# monte-carlo-script

Monte-Carlo Script is a domain-specific expression language and its interpreter, designed for building plots and 
histograms of modeling functions for Monte-Carlo method. 

## Building

Prerequisites:

1. The following packages have to be installed: `UUID` and `Java.Runtime`. 
2. Recommended version of CMake is 3.15.3 or higher.
3. Recommended compiler is GCC 8.3.0 or higher.

Building:

```bash
mkdir cmake-build-debug
cd cmake-build-debug
cmake ..
make
```


