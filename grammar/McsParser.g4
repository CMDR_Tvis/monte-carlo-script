parser grammar McsParser;

options { tokenVocab = McsLexer; }

program
    : expression
    ;

expression
    : expression POW expression #Exponentiation
    | expression MULT expression #Multiplication
    | expression DIV expression #Division
    | expression ADD expression #Addition
    | expression SUB expression #Subtraction
    | sign DOUBLE #Number
    | sign E #E
    | sign PI #Pi
    | sign ARGUMENT #Argument
    | sign LPAREN expression RPAREN #Subexpression
    | sign IDENTIFIER LPAREN expression (COMMA expression)* RPAREN #FunctionCall
    ;

sign
    : (SUB | ADD)*
    ;
