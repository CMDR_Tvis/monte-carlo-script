lexer grammar McsLexer;

fragment DEC_DIGITS
    : ('0'..'9')+
    ;

fragment LETTER
    : 'a'..'z'
    ;

ARGUMENT
    : 'x'
    ;

PI
    : 'pi'
    ;

E
    : 'e'
    ;

IDENTIFIER
    :
    ( LETTER
    | UNDERSCORE
    )
    ( LETTER
    | '0'..'9'
    | UNDERSCORE
    ) *
    ;

DOUBLE
    : DEC_DIGITS? '.' DEC_DIGITS
    | DEC_DIGITS
    ;

MULT
    : '*'
    ;

MOD
    :'%'
    ;

DIV
    : '/'
    ;

ADD
    : '+'
    ;

SUB
    : '-'
    ;

POW
    : '^'
    ;

COMMA
    : ','
    ;

UNDERSCORE
    : '_'
    ;

LPAREN
    : '('
    ;

RPAREN
    : ')'
    ;
